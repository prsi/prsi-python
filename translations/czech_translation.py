import json
from abc import ABC

from card.card import Card
from card.cardcolor import CardColor
from card.cards import Cards
from card.cardtype import CardType
from translations.translation import Translation
from io import open


def get_type_name(type: CardType) -> str:
    if type == CardType.MENIC:
        return 'měnič'
    elif type == CardType.KRAL:
        return 'král'
    elif type == CardType.ESO:
        return 'ESO'
    elif type == CardType.SVRSEK:
        return 'svršek'
    elif type == CardType.SEVEN:
        return 'sedma'
    elif type == CardType.EIGHT:
        return 'osma'
    elif type == CardType.NINE:
        return 'devítka'
    else:
        return 'desítka'


class CzechTranslation(Translation, ABC):
    def __init__(self):
        super().__init__('Czech', 'Čeština')

        print()

    def card_translation(self) -> dict[Card, str]:
        map = {}

        for card in Cards.cards():
            color_name = self.get_card_color_name(card)
            type_name = get_type_name(card.type)

            map[card] = f'{color_name} {type_name}'

        return map

    def card_color_translation(self) -> dict[CardColor, str]:
        return dict(map(lambda color: (color, self.get_color_name(color)), list(CardColor)))

    def string_translations(self) -> dict[str, str]:
        f = open('translations/czech.json', mode='r', encoding='UTF-8')
        data = json.load(f)

        f.close()

        return data

    @staticmethod
    def get_card_color_name(card: Card) -> str:
        if card.type == CardType.ESO \
                or card.type == CardType.MENIC \
                or card.type == CardType.SVRSEK \
                or card.type == CardType.KRAL:

            if card.color == CardColor.KULE:
                return 'kuloví'
            elif card.color == CardColor.LISTY:
                return 'zelený'
            elif card.color == CardColor.ZALUDY:
                return 'žaludoví'
            else:
                return 'červený'
        else:
            if card.color == CardColor.KULE:
                return 'kulová'
            elif card.color == CardColor.LISTY:
                return 'zelená'
            elif card.color == CardColor.ZALUDY:
                return 'žaludová'
            else:
                return 'červená'

    @staticmethod
    def get_color_name(card_color: CardColor) -> str:
        if card_color == CardColor.KULE:
            return 'kule'
        elif card_color == CardColor.LISTY:
            return 'listy'
        elif card_color == CardColor.ZALUDY:
            return 'žaludy'
        else:
            return 'červený'
