from enum import Enum

from translations.czech_translation import CzechTranslation


class Languages(Enum):
    EN = 0
    CZ = 1

    @staticmethod
    def get_translation(translation):
        if translation == 1:
            return CzechTranslation
