from player.player import Player


class Players:
    def __init__(self, players: set[Player]):
        self.players = players
        self.remaining_players = players.copy()

    def check_players(self):
        not_playing_players = []

        for player in self.remaining_players:
            if not player.is_playing():
                not_playing_players.append(player)

        for player in not_playing_players:
            self.remaining_players.remove(player)

    def clear_players_decks(self):
        for player in self.remaining_players:
            player.clear_deck()

    def playing_players_count(self):
        count = 0

        for player in self.remaining_players:
            if player.is_playing():
                count += 1

        return count

    def winner(self) -> Player:
        if len(self.remaining_players) == 1:
            return self.remaining_players[0]
