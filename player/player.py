from card.card import Card


class Player:
    def __init__(self, name: str):
        self.name = name

        self.deck: list[Card] = []

    def clear_deck(self):
        self.deck.clear()

    def is_playing(self) -> bool:
        return len(self.deck) > 0

    def add_card_to_deck(self, card: Card):
        self.deck.append(card)

    def remove_card_from_deck(self, card: Card):
        self.deck.remove(card)
