from enum import Enum


class CardColor(Enum):
    KULE = 0
    CERVENY = 1
    LISTY = 2
    ZALUDY = 3
