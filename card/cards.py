from card.card import Card
from card.cardcolor import CardColor
from card.cardtype import CardType


class Cards:
    cached_cards = None

    @staticmethod
    def cards() -> list[Card]:
        if Cards.cached_cards is None:
            types = list(CardType)
            colors = list(CardColor)

            cached_cards = []

            for type in types:
                for color in colors:
                    cached_cards.append(Card(color, type))

            Cards.cached_cards = cached_cards

        return Cards.cached_cards
